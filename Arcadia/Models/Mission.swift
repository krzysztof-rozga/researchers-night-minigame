//
//  Mission.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 15/10/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import Foundation

struct Mission {
    
    static let numberOfMissions = 4
    
    var explosive: Explosive
    var language: Language
    
    init?(from number: Int) {
        switch number {
        case 0:
            explosive = .color
            language = .english
        case 1:
            explosive = .color
            language = .spanish
        case 2:
            explosive = .number
            language = .english
        case 3:
            explosive = .number
            language = .spanish
        default:
            return nil
        }
    }
    
}
