//
//  Explosive.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 09/08/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import Foundation

enum Explosive {
    
    case color, number
    
    var path: String {
        switch self {
            case .color: return "art.scnassets/ColorBomb/ColorBomb.dae"
            case .number: return "art.scnassets/NumBomb/NumBomb.dae"
        }
    }
    
    var triggers: [String] {
        switch self {
            case .color: return ["BtnRed", "BtnGreen", "BtnYellow", "BtnBlue", "BtnWhite"]
            case .number: return (1...12).map({"Btn\($0)"})
        }
    }
    
    var hints: [String] {
        switch self {
            case .color: return ["czerwony", "zielony", "żółty", "niebieski", "biały"]
            case .number:
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: Language.polish.iso639_1)
                return (1...12).map{ formatter.string(from: NSNumber(value: $0))! }
        }
    }
    
    func objectives(for language: Language) -> [String] {
        switch self {
        case .color:
            switch language {
                case .spanish: return ["rojo", "verde", "amarillo", "azul", "blanco"]
                default: return ["red", "green", "yellow", "blue", "white"]
            }
        case .number:
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: language.iso639_1)
            formatter.numberStyle = .spellOut
            return (1...12).map{ formatter.string(from: NSNumber(value: $0))! }
        }
    }
}
