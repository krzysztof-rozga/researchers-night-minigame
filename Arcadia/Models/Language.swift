//
// Created by Krzysztof Rózga on 13/01/2018.
// Copyright (c) 2018 Krzysztof Rózga. All rights reserved.
//

import Foundation

enum Language {
    
    case polish, english, german, spanish
    
    var iso639_1: String {
        switch self {
            case .polish: return "pl"
            case .english: return "en"
            case .german: return "de"
            case .spanish: return "es"
        }
    }
    
    var iso639_2: String {
        switch self {
            case .polish: return "pol"
            case .english: return "eng"
            case .german: return "deu"
            case .spanish: return "spa"
        }
    }
}

extension Language {
    init? (iso code: String) {
        switch code.lowercased() {
            case "pl": self = .polish
            case "en": self = .english
            case "de": self = .german
            case "es": self = .spanish
            default: return nil
        }
    }
}

