//
//  CABasicAnimationExtension.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 15/10/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import Foundation
import UIKit

extension CABasicAnimation {
    
    static func shake(from: CGPoint, to: CGPoint) -> CABasicAnimation {
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 40
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: from)
        animation.toValue = NSValue(cgPoint: to)
        
        return animation
    }
    
}
