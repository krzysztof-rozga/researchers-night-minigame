//
//  float4x4Extension.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 08/08/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import simd

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}
