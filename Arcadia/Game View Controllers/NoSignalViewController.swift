//
//  NoSignalViewController.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 15/10/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import UIKit

class NoSignalViewController: UIViewController {
    
    typealias DismissCallback = (() -> ())

    var dismissParent: DismissCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tappedQuitButton(_ sender: Any) {
        dismissParent?()
    }
}
