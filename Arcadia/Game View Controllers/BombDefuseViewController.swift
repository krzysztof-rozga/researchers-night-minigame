//
//  BombDefuseViewController.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 08/08/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class BombDefuseViewController: UIViewController {

    var mission: Mission!
    
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var objectiveLabel: UILabel!
    @IBOutlet weak var numberOfHintsLabel: UILabel!
    @IBOutlet weak var hintView: UIVisualEffectView!
    @IBOutlet weak var objectiveBlurView: UIVisualEffectView!

    private let grid = UIImage(named: "grid")!
    private var planeIsSet = false
    private var currentHint = ""
    private var numberOfHints = 3
    private var defuse: Defuse?
    private var explosiveScene: SCNScene?
    private var scanHintView: UIView?
    
    private let explosionParticle = SCNParticleSystem(named: "ExplosionParticleSystem", inDirectory: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        configureGestures()
        configureDefuse()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureScene()
        configureLightning()
        load3DModels()
        numberOfHintsLabel.text = String(numberOfHints)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presentScanHint()
    }

    @IBAction func tappedHelpButton(_ sender: Any) {
        if numberOfHints > 0 {
            objectiveLabel.text = currentHint
            numberOfHints -= 1
            numberOfHintsLabel.text = String(numberOfHints)
            if numberOfHints == 0 {
                UIView.animate(withDuration: 0.5) {
                    self.hintView.alpha = 0.0
                }
            }
        }
    }
    
}

// MARK: - Defuse Delegate

extension BombDefuseViewController: DefuseDelegate {
    
    func didLoad(objective: String, hint: String) {
        AudioServicesPlayAlertSound(1115)
        objectiveLabel.text = objective
        currentHint = hint
    }
    
    func didFail() {
        sceneView.scene.rootNode.childNodes.forEach({
            $0.addParticleSystem(explosionParticle!)
        })
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        let animation = CABasicAnimation.shake(
            from: CGPoint(x: sceneView.center.x - 18, y: sceneView.center.y),
            to: CGPoint(x: sceneView.center.x + 18, y: sceneView.center.y)
        )
        sceneView.layer.add(animation, forKey: "position")
        let gameOverViewController = NoSignalViewController.init(nibName: "NoSignalViewController", bundle: nil)
        self.addChild(gameOverViewController)
        gameOverViewController.view.alpha = 0.0
        gameOverViewController.view.frame = self.view.frame
        gameOverViewController.dismissParent = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        self.view.addSubview(gameOverViewController.view)
        gameOverViewController.didMove(toParent: self)
        UIView.animate(withDuration: 1.0) {
            gameOverViewController.view.alpha = 1.0
        }
    }
    
    func didDefuse() {
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK: - AR user interaction

extension BombDefuseViewController {
    
    @objc func didTap(withGestureRecognizer recognizer: UIGestureRecognizer) {
    
        let tapLocation = recognizer.location(in: sceneView)
        
        if planeIsSet {
            for hit in sceneView.hitTest(tapLocation, options: nil) {
                if let nodeName = hit.node.name {
                    defuse?.select(trigger: nodeName)
                }
            }
        } else {
            guard let hitTestResult = sceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent).first else { return }
            
            guard let explosiveScene = explosiveScene else { return }
            let explosiveNode = SCNNode()
            explosiveScene.rootNode.childNodes.forEach({ explosiveNode.addChildNode($0) })
            let translation = hitTestResult.worldTransform.translation
            
            explosiveNode.position = SCNVector3(
                translation.x,
                translation.y + 0.1,
                translation.z
            )
            
            explosiveNode.scale = SCNVector3(0.2, 0.2, 0.2)
            
            sceneView.scene.rootNode.addChildNode(explosiveNode)
            planeIsSet = true
            self.objectiveBlurView.isHidden = false
            self.hintView.isHidden = false
        }
    }
}


// MARK: - ARSCNViewDelegate

extension BombDefuseViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard !planeIsSet,let planeAnchor = anchor as? ARPlaneAnchor else { return }
        DispatchQueue.main.async {
            self.scanHintView?.removeFromSuperview()
        }
        let plane = SCNPlane(
            width: CGFloat(planeAnchor.extent.x),
            height: CGFloat(planeAnchor.extent.z))
        
        plane.materials.first?.diffuse.contents = UIColor.cyan.withAlphaComponent(0.2)
        plane.materials.first?.diffuse.contents = grid
        
        let planeNode = SCNNode(geometry: plane)
        
        planeNode.position = SCNVector3(
            CGFloat(planeAnchor.center.x),
            CGFloat(planeAnchor.center.y),
            CGFloat(planeAnchor.center.z))
        
        planeNode.eulerAngles.x = -.pi / 2
        node.addChildNode(planeNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
            let planeNode = node.childNodes.first,
            let plane = planeNode.geometry as? SCNPlane
            else { return }
        
        plane.width = CGFloat(planeAnchor.extent.x)
        plane.height = CGFloat(planeAnchor.extent.z)
        if planeIsSet {
            plane.materials.forEach({ $0.diffuse.contents = UIColor.clear })
        }
        
        planeNode.position = SCNVector3(
            CGFloat(planeAnchor.center.x),
            CGFloat(planeAnchor.center.y),
            CGFloat(planeAnchor.center.z))
    }
}


// MARK: - Preparation

extension BombDefuseViewController {
    
    private func configureScene() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    private func configureLightning() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    private func load3DModels() {
        self.explosiveScene = SCNScene(named: mission.explosive.path)
    }
    
    private func configureGestures() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    private func configureDefuse() {
        defuse = Defuse(mission: mission)
        defuse?.delegate = self
    }
    
    private func presentScanHint() {
        guard scanHintView == nil, let viewToAdd = Bundle.main.loadNibNamed("ScanHintView", owner: self, options: nil)?.first as? UIView else { return }
        viewToAdd.alpha = 0.0
        viewToAdd.frame.size = CGSize(width: self.view.frame.width - 30.0, height: 100.0)
        viewToAdd.center = self.view.center
        scanHintView = viewToAdd
        self.view.addSubview(viewToAdd)
        UIView.animate(withDuration: 0.3) {
            viewToAdd.alpha = 1.0
        }
    }
}
