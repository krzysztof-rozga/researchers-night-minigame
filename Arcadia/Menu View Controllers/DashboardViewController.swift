//
//  DashboardViewController.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 08/08/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var outerImageView: UIImageView!
    @IBOutlet weak var innerImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        outerImageView.rotate(duration: 28.0)
        innerImageView.rotate(duration: 20.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        outerImageView.stopRotating()
        innerImageView.stopRotating()
    }
    
    private func startBombDefuse(with mission: Mission) {
        let viewControllerToPresent = BombDefuseViewController(nibName: "BombDefuseViewController", bundle: nil)
        viewControllerToPresent.mission = mission
        self.present(viewControllerToPresent, animated: true, completion: nil)
    }
}

extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Mission.numberOfMissions
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "missionCell") as! MissionCell
        cell.missionNumber = indexPath.row + 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let mission = Mission(from: indexPath.row) else { return }
        startBombDefuse(with: mission)
    }
}
