//
//  MissionCell.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 28/09/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import UIKit

class MissionCell: UITableViewCell {

    var missionNumber: Int = 0 {
        didSet {
            self.missionLabel.text = "\(NSLocalizedString("mission", comment: "")) \(missionNumber)"
        }
    }
    
    @IBOutlet private weak var missionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
