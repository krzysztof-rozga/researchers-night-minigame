//
//  Defuse.swift
//  Arcadia
//
//  Created by Krzysztof Rózga on 15/10/2018.
//  Copyright © 2018 Krzysztof Rózga. All rights reserved.
//

import Foundation

protocol DefuseDelegate: class {
    func didLoad(objective: String, hint: String)
    func didFail()
    func didDefuse()
}

class Defuse {
    
    weak var delegate: DefuseDelegate? { didSet { nextObjective() }}
    
    private var currentTrigger: String?
    private var triggers: [String]
    private var objectives: [String]
    private var hints: [String]
    
    init(mission: Mission) {
        let order = (0..<mission.explosive.triggers.count).shuffled()
        objectives = order.map({ mission.explosive.objectives(for: mission.language)[$0] })
        triggers = order.map({ mission.explosive.triggers[$0] })
        hints = order.map({ mission.explosive.hints[$0] })
    }
    
    func select(trigger: String) {
        if triggers.contains(trigger) {
            delegate?.didFail()
        } else if trigger == currentTrigger {
            nextObjective()
        }
    }
    
    private func nextObjective() {
        guard !objectives.isEmpty, !hints.isEmpty else { delegate?.didDefuse(); return }
        currentTrigger = triggers.removeFirst()
        delegate?.didLoad(objective: objectives.removeFirst(), hint: hints.removeFirst())
    }
}
